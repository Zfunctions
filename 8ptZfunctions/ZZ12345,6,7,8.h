* using ZZ12345,6,7,8-ansatz and imposing the monodromies on 1,2,3,4
* leads to an answer that contains many parameters left, but all multiply
* G4. So effectively there is only one parameter left

* Imposing shuffle fixes x1 = 0 below.

	id ZZ(i1?,i2?,i3?,i4?,i5?,[c],i6?,[c],i7?,[c],i8?) =

*       + x1 * (
*          + G4
*          )

       + g1(i1,i2)*g1(i2,i3)*g1(i3,i4)*g1(i4,i5)
          + g1(i1,i2)*g1(i2,i3)*g2(i1,i4)
          - g1(i1,i2)*g1(i2,i3)*g2(i1,i5)
          + g1(i1,i2)*g1(i2,i3)*g2(i4,i5)
          + g1(i1,i2)*g1(i3,i4)*g2(i1,i4)
          - g1(i1,i2)*g1(i3,i4)*g2(i1,i5)
          + g1(i1,i2)*g1(i3,i4)*g2(i3,i5)
          + g1(i1,i2)*g1(i4,i5)*g2(i1,i3)
          - g1(i1,i2)*g1(i4,i5)*g2(i1,i5)
          + g1(i1,i2)*g1(i4,i5)*g2(i3,i5)
          - 2*g1(i1,i2)*g3(i1,i4)
          + 2*g1(i1,i2)*g3(i1,i5)
          - g1(i1,i2)*g3(i3,i4)
          - 2*g1(i1,i2)*g3(i3,i5)
          + g1(i2,i3)*g1(i3,i4)*g2(i1,i4)
          - g1(i2,i3)*g1(i3,i4)*g2(i1,i5)
          + g1(i2,i3)*g1(i3,i4)*g2(i2,i5)
          + g1(i2,i3)*g1(i4,i5)*g2(i1,i3)
          - g1(i2,i3)*g1(i4,i5)*g2(i1,i5)
          + g1(i2,i3)*g1(i4,i5)*g2(i2,i5)
          - 2*g1(i2,i3)*g3(i1,i4)
          + 2*g1(i2,i3)*g3(i1,i5)
          - g1(i2,i3)*g3(i2,i4)
          - 2*g1(i2,i3)*g3(i2,i5)
          + g1(i3,i4)*g1(i4,i5)*g2(i1,i2)
          - g1(i3,i4)*g1(i4,i5)*g2(i1,i5)
          + g1(i3,i4)*g1(i4,i5)*g2(i2,i5)
          - 2*g1(i3,i4)*g3(i1,i4)
          + 2*g1(i3,i4)*g3(i1,i5)
          - g1(i3,i4)*g3(i2,i4)
          - 2*g1(i3,i4)*g3(i2,i5)
          - 2*g1(i4,i5)*g3(i1,i3)
          + 2*g1(i4,i5)*g3(i1,i5)
          - g1(i4,i5)*g3(i2,i3)
          - 2*g1(i4,i5)*g3(i2,i5)
          + g2(i1,i2)*g2(i1,i4)
          - g2(i1,i2)*g2(i1,i5)
          + g2(i1,i2)*g2(i4,i5)
          + g2(i1,i4)*g2(i2,i3)
          + g2(i1,i4)*g2(i3,i4)
          - g2(i1,i5)*g2(i2,i3)
          - g2(i1,i5)*g2(i3,i4)
          - g2(i1,i5)*g2(i4,i5)
          + g2(i2,i3)*g2(i2,i5)
          + g2(i2,i5)*g2(i3,i4)
          + g2(i2,i5)*g2(i4,i5)
          + 3*g4(i1,i4)
          - 3*g4(i1,i5)
          + 3*g4(i2,i4)
          + 3*g4(i2,i5)
         ;

